//
//  ViewController.swift
//  AppBitcoin
//
//  Created by iossenac on 13/11/18.
//  Copyright © 2018 iossenac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        buscarPreco();
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet weak var lblValor: UILabel!
    @IBOutlet weak var btnAtualizar: UIButton!
    @IBAction func atualizar(_ sender: Any) {
        buscarPreco();
    }
    
    func buscarPreco(){
        self.setValor(0)
        btnAtualizar.setTitle("Atualizando...", for: .normal)
        //self.view.setNeedsDisplay()
        
        if let url = URL(string: "https://www.blockchain.com/pt/ticker"){
            let tarefa = URLSession.shared.dataTask(with: url) {
                (dados,requisicao,erro) in
                if erro == nil{
                    do {
                        if let objetoJSON = try JSONSerialization.jsonObject(with: dados!, options: []) as? [String: Any] {
                            if let brl = objetoJSON["BRL"] as? [String: Any] {
                                if let preco = brl["buy"] as? NSNumber{
                                    DispatchQueue.main.sync {
                                        self.setValor(preco)
                                        //self.view.setNeedsDisplay()
                                    }
                                }
                            }
                        }
                    } catch {
                        print ("Erro ao tentar ler JSON")
                    }
                } else {
                    print ("Erro na request")
                }
                DispatchQueue.main.sync {
                    self.btnAtualizar.setTitle("Atualizar", for: .normal)
                  //  self.displa
                  //  self.view.setNeedsDisplay()
                    
                }
            }
            tarefa.resume()
        }
    }
    
    func setValor(_ preco : NSNumber){
        
        
        lblValor.text = "R$ " + formatarValor(preco);
    }
    
    func formatarValor (_ preco : NSNumber ) -> String {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.locale = Locale(identifier: "pt_BR")
        if let precoFinal =  nf.string(from: preco){
            return precoFinal
        }
        return "0,00"
    }

}

